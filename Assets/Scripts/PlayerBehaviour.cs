﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class PlayerBehaviour : MonoBehaviour
{
 
    public float speed;
    private Vector2 axis;
    public Vector2 limits;
 
    private float shootTime=0;
 
    public Weapon weapon;
 
    public Propeller prop;
   
   GameObject grafico;
   public ParticleSystem ps;
   public AudioSource soundExplosion;
   private bool iAmDead = false;
   public int lives = 3;
   [SerializeField] Collider2D collider;

   //ANIMACION

   Animator anim;
   
    // Update is called once per frame
    void Awake(){
        grafico = transform.GetChild(0).gameObject;
        anim = GetComponent<Animator>();
    }
    void Update () {
        //si estoy muerto no puedo hacer nada
        if(iAmDead){
            return;
        }


        
        shootTime += Time.deltaTime;
 //Movimiento Nave
        transform.Translate (axis * speed * Time.deltaTime);
 //Limites
        if (transform.position.x > limits.x) {
            transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
        }else if (transform.position.x < -limits.x) {
            transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
        }
 
        if (transform.position.y > limits.y) {
            transform.position = new Vector3 (transform.position.x, limits.y, transform.position.z);
        }else if (transform.position.y < -limits.y) {
            transform.position = new Vector3 (transform.position.x, -limits.y, transform.position.z);
        }
 //Propulsor
        if(axis.x>0){
            prop.BlueFire();
        }else if(axis.x<0){
            prop.RedFire();
        }else{
            prop.Stop();
        }
    }
 
    public void ActualizaDatosInput(Vector2 currentAxis){
        axis = currentAxis;
    }
 
    public void SetAxis(float x, float y){
        axis = new Vector2(x,y);
    }

    public void SetAxis(Vector2 currentAxis){
        axis = currentAxis;
    }
 
    public void Shoot(){
        if(!iAmDead && shootTime>weapon.GetCadencia()){
            shootTime = 0f;
            weapon.Shoot();
        }
    }
    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "meteor"){
            StartCoroutine(DestroyShip());
        }
        else if(other.tag == "Enemy" || other.tag == "EnemyBullet"){
            StartCoroutine(DestroyShip());
        }
    }

    IEnumerator DestroyShip(){
        iAmDead = true;
        lives --;
        grafico.SetActive(false);
        collider.enabled = false;
        ps.Play();
        soundExplosion.Play();
        prop.gameObject.SetActive(false);
        anim.SetBool("Muerto", true);

        yield return new WaitForSeconds(1.0f);
        if(lives > 0){
            StartCoroutine(Inmortal());
        }

    }

    IEnumerator Inmortal(){
        iAmDead = false;
        grafico.SetActive(true);
        prop.gameObject.SetActive(true);
        anim.SetBool("Muerto",false);
        for (int i = 0; i <= 15; i++) {
            grafico.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            grafico.SetActive(true);
            yield return new WaitForSeconds(0.1f);
        }
        collider.enabled = true;
    }
}