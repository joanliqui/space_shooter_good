﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorManager : MonoBehaviour
{
    public GameObject meteorPrefab;
    public float timeLaunchMeteor;

    private float currentTime = 0;
    private float randomPosition;
    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        randomPosition = Random.Range( -5, 5);

        if(currentTime >=timeLaunchMeteor){
            currentTime = 0;
            Instantiate(meteorPrefab, new Vector3(this.transform.position.x, randomPosition, 0), Quaternion.identity, this.transform);
        }
    }
}
