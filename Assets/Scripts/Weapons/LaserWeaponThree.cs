﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserWeaponThree : Weapon
{

    public GameObject laserBullet;
    public GameObject laserBulletVertical;
    public GameObject laserBulletVerticalDown;
    public AudioSource sound;
    public float cadencia;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate (laserBullet, this.transform.position, Quaternion.identity, null);
        Instantiate (laserBulletVertical, this.transform.position, Quaternion.identity, null);
        Instantiate (laserBulletVerticalDown, this.transform.position, Quaternion.identity, null);
        sound.Play();

    }
}
