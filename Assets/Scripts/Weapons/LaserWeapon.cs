﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserWeapon : Weapon
{

    public GameObject laserBullet;

    public AudioSource sound;
    public float cadencia;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        sound.Play();
        Instantiate (laserBullet, this.transform.position, Quaternion.identity, null);
    }
}
