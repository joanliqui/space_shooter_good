﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] GameObject[] formacion;
    public float timeLauncherEnemy;
    private float currentTime = 0;
    private float randomPosition;

    private int randomFormacion;

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        randomPosition = Random.Range( -5, 5);
        randomFormacion = Random.Range(0, formacion.Length);

        if(currentTime >= timeLauncherEnemy){
            currentTime = 0;
            Instantiate(formacion[randomFormacion], new Vector3(this.transform.position.x, randomPosition, 0), Quaternion.identity, this.transform);
        }
    }
}
