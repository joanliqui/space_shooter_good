﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CascadeBullet : MonoBehaviour
{
    public float speed;
    public float timeCascade;
    public GameObject cascadeBullet;

    private float currentTimeCascade = 0;
    
    // Update is called once per frame
    void Update () {
        transform.Translate (speed * Time.deltaTime,0, 0);
        currentTimeCascade += Time.deltaTime;
        
        if(currentTimeCascade > timeCascade ){

            Instantiate (cascadeBullet, this.transform.position, Quaternion.identity, null);
            GameObject go = Instantiate (cascadeBullet, this.transform.position, Quaternion.identity, null);
            go.transform.Rotate(0,0,30);
            GameObject go2 = Instantiate (cascadeBullet, this.transform.position, Quaternion.identity, null);
            go2.transform.Rotate(0,0,-30);

            Destroy(gameObject);
        }


    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish") {
            Destroy (gameObject);
        }
    }

}
