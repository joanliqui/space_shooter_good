﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreBoomBullet : MonoBehaviour
{
    public float speed;
    public float timeToBoom;
    public GameObject BoomBullet;
    private float currentTime = 0;

    
    // Update is called once per frame
    void Update () {
        transform.Translate (speed * Time.deltaTime,0, 0);
        currentTime += Time.deltaTime;

        if(currentTime >= timeToBoom){
            Instantiate(BoomBullet,this.transform.position, Quaternion.identity, null);
            GameObject Boom2 = Instantiate(BoomBullet, this.transform.position, Quaternion.identity, null);
            Boom2.transform.Rotate(0 ,0, 90);
            GameObject Boom3 = Instantiate(BoomBullet,this.transform.position, Quaternion.identity, null);
            Boom3.transform.Rotate(0, 0, 180);
            GameObject Boom4 = Instantiate(BoomBullet, this.transform.position, Quaternion.identity, null);
            Boom4.transform.Rotate( 0, 0, 270);
            
            Destroy(gameObject);
        }
    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish") {
            Destroy (gameObject);
        }
    }

}
