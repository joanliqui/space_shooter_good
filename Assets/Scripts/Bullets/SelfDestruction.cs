﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruction: MonoBehaviour
{
    public float speed;
    public float timeDestruction;
    private float currentTime =0;

    
    // Update is called once per frame
    void Update () {
        transform.Translate (speed * Time.deltaTime,0, 0);
        currentTime += Time.deltaTime;
        if(currentTime >= timeDestruction){
            Destroy(gameObject);
        }
    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish") {
            Destroy (gameObject);
        }
    }

}
