﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YForkBullet : MonoBehaviour
{
    public float speed;
    public float timeToTurn;
    private float currentTimeFork = 0;
    public float rotation;
    // Update is called once per frame
    void Update () {
        //Se modifica la posicion de la bala en cada frame
        transform.Translate (speed * Time.deltaTime,0, 0);
        currentTimeFork += Time.deltaTime;

        if(currentTimeFork >= timeToTurn){
        this.transform.Rotate( 0,0, rotation);
        
        }
        
    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish") {
            Destroy (gameObject);
        }
    }

}
