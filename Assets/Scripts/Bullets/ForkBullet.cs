﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForkBullet : MonoBehaviour
{
    public float speed;
    public float timeFork;
    //public float goAround;
    public GameObject forkBullet;
    public GameObject forkBulletUp;
    public GameObject forkBulletDown;

    private float currentTimeFork = 0;
    // Update is called once per frame
    void Update () {
        //Se modifica la posicion de la bala en cada frame
        transform.Translate (speed * Time.deltaTime,0, 0);
        currentTimeFork += Time.deltaTime;
        
        if(currentTimeFork > timeFork ){
            //Recto Bullet
            Instantiate (forkBullet, this.transform.position, Quaternion.identity, null);
            //ArribaBullet
            Instantiate (forkBulletUp, this.transform.position, Quaternion.identity, null);
            forkBulletUp.transform.Rotate(0,0,90);
            //AbajoBullet
            Instantiate (forkBulletDown, this.transform.position, Quaternion.identity, null);
            forkBulletDown.transform.Rotate(0,0,-90);

            Destroy(gameObject);
        }


    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish") {
            Destroy (gameObject);
        }
    }

}
