﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    private bool isPaused = false;

    [SerializeField] GameObject canvas;

    public void Quit(){
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("MainMenu");
    }
    void Update()
    {
        if(!isPaused && Input.GetKeyDown(KeyCode.Escape)){
            ActivePause();
        } else if(isPaused && Input.GetKeyDown(KeyCode.Escape)){
            Continue();
        }
    }

    public void Continue(){
        canvas.SetActive(false);
        Time.timeScale = 1.0f;
        isPaused = false;
    }
    void ActivePause(){
        canvas.SetActive(true);
        isPaused = true;
        Time.timeScale = 0;
    }
}
