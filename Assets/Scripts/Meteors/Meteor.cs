﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    Vector2 speed;
    Transform graphics;
    public ParticleSystem ps;

    public AudioSource soundExplosion;

    public GameObject instanciateMeteorit;

    public ScoreManager smObject;
     
    // Start is called before the first frame update
    void Awake() {
      graphics = transform.GetChild(0);

        for(int i  = 0; i < graphics.childCount; i++){
            graphics.GetChild(i).gameObject.SetActive(false);
        }
    int seleccionado = Random.Range(0, graphics.childCount);
    graphics.GetChild(seleccionado).gameObject.SetActive(true);

    speed.x = Random.Range(-7 , -1);
    speed.y = Random.Range(-3 , 3);

    smObject = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();

    }
    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed * Time.deltaTime);
        graphics.Rotate(0, 0, 100 * Time.deltaTime);

        
    }
    
    public void OnTriggerEnter2D(Collider2D other){
            if(other.tag == "Finish"){
                Destroy(gameObject);
            }

           else if (other.tag == "bullet") {
                StartCoroutine(DestroyMeteor());
          }
    }

    IEnumerator DestroyMeteor(){
        graphics.gameObject.SetActive(false);
        Destroy(GetComponent<BoxCollider2D>());
        soundExplosion.Play();
        ps.Play();
        InstantiateMeteorits();
        yield return new WaitForSeconds(1.0f);
        Destroy(this.gameObject);
    }

    public virtual void InstantiateMeteorits(){
        Instantiate (instanciateMeteorit, this.transform.position, Quaternion.identity, null);
        Instantiate (instanciateMeteorit, this.transform.position, Quaternion.identity, null);
        Instantiate (instanciateMeteorit, this.transform.position, Quaternion.identity, null);
        Instantiate (instanciateMeteorit, this.transform.position, Quaternion.identity, null);
        smObject.AddScore(25);

    }
}
