﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] GameObject[] lives;
    [SerializeField] Text score;
 
    private int scoreInt;
 
    // Start is called before the first frame update
    void Start()
    {
        scoreInt = 0;
        score.text = scoreInt.ToString("000000");
    }
 
    public void AddScore(int value){
        scoreInt+=value;
        score.text = scoreInt.ToString("000000");
    }
}
