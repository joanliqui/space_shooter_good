﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemi1Behaviour : MonoBehaviour
{
    private float speed = -5;
    [SerializeField] GameObject enemieBullet;

    private ScoreManager sm;

     void Awake() {
        StartCoroutine("Disparo");
        sm = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
    }
    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed * Time.deltaTime,0,0);
    }
    IEnumerator Disparo(){
        while(true){
        yield return new WaitForSeconds(1.5f);
        speed = 0;
        Instantiate (enemieBullet, this.transform.position, Quaternion.identity, null);
        yield return new WaitForSeconds(0.5f);
        speed = -5;
        }
    }

    public void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Finish"){
            Destroy(gameObject);
        }
        else if (other.tag == "Ship" || other.tag =="bullet"){
            Destroy(gameObject);
            sm.AddScore(100);
        }
    }
}
